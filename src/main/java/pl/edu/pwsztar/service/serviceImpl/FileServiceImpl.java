package pl.edu.pwsztar.service.serviceImpl;

import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import pl.edu.pwsztar.domain.converter.Converter;
import pl.edu.pwsztar.domain.dto.FileDto;
import pl.edu.pwsztar.domain.entity.Movie;
import pl.edu.pwsztar.domain.files.GenerateTxt;
import pl.edu.pwsztar.domain.repository.MovieRepository;
import pl.edu.pwsztar.service.FileService;

import java.io.*;
import java.util.List;

@Service
public class FileServiceImpl implements GenerateTxt, FileService {
    private final MovieRepository movieRepository;
    private final Converter<List<Movie>, List<FileDto>> converter;

    public FileServiceImpl(MovieRepository movieRepository, Converter<List<Movie>, List<FileDto>> converter){
        this.movieRepository = movieRepository;
        this.converter = converter;
    }

    public List<FileDto> getSortedMovies(){
        return converter.convert(movieRepository.findAll(Sort.by("year").descending()));
    }

    @Override
    public InputStreamResource toTxt(List<FileDto> fileDto) throws IOException{
        File f=File.createTempFile("tmp", ".txt");
        FileOutputStream fos=new FileOutputStream(f);

        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));

        for(FileDto file: fileDto){
            bw.write(file.getYear() + " " + file.getTitle());
            bw.newLine();
        }

        bw.close();

        fos.flush();
        fos.close();

        InputStream stream = new FileInputStream(f);
        return new InputStreamResource(stream);
    }
}

