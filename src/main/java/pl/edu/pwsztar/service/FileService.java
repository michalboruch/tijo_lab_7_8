package pl.edu.pwsztar.service;

import pl.edu.pwsztar.domain.dto.FileDto;

import java.util.List;

public interface FileService {
    List<FileDto> getSortedMovies();
}
