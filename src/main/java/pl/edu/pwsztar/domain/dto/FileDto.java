package pl.edu.pwsztar.domain.dto;

public class FileDto {
    private int year;
    private String title;

    public FileDto(Builder builder){
        year = builder.year;
        title = builder.title;
    }

    public int getYear(){
        return year;
    }

    public String getTitle(){
        return title;
    }

    public static final class Builder{
        private int year;
        private String title;

        public Builder(){}

        public Builder year(int year){
            this.year = year;
            return this;
        }

        public Builder title(String title){
            this.title = title;
            return this;
        }

        public FileDto build(){
            return new FileDto(this);
        }
    }
}
