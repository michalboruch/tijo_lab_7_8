package pl.edu.pwsztar.domain.mapper;

import org.springframework.stereotype.Component;
import pl.edu.pwsztar.domain.converter.Converter;
import pl.edu.pwsztar.domain.dto.FileDto;
import pl.edu.pwsztar.domain.entity.Movie;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class FileListMapper implements Converter<List<Movie>, List<FileDto>> {
    @Override
    public List<FileDto> convert(List<Movie> item){
        return item.stream()
                .map(movie ->
                        new FileDto.Builder()
                        .title(movie.getTitle())
                        .year(movie.getYear())
                        .build())
                .collect(Collectors.toList());
    }
}
